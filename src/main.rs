#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![allow(clippy::non_ascii_literal)]

#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate serde_derive;

mod libcw;

use std::collections::HashMap;
use std::convert::TryInto;
use std::env;
use std::fmt::{self, Display};
use std::fs::{self, DirEntry, File};
use std::io::{self, BufRead, BufReader, Read, Write};
use std::path::Path;
use std::process::{Command, Stdio};
use std::thread;
use std::time::Duration;

mod errors {
    // Create the Error, ErrorKind, ResultExt, and Result types
    error_chain! {}
}

use crate::errors::*;

use lazy_static::lazy_static;

// const CONFIG_DIR: &'static str = "/etc/ananicy.d/";
macro_rules! CONFIG_DIR {
    () => {
        "/etc/ananicy.d/"
    };
}

#[derive(Clone, Copy, Deserialize, PartialEq)]
enum Ioclass {
    #[serde(rename = "none")]
    None = 0,
    #[serde(rename = "realtime")]
    Realtime = 1,
    #[serde(rename = "best-effort")]
    BestEffort = 2,
    #[serde(rename = "idle")]
    Idle = 3,
}

impl Display for Ioclass {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::None => "none",
                Self::Realtime => "realtime",
                Self::BestEffort => "best_effort",
                Self::Idle => "idle",
            }
        )
    }
}

#[derive(Clone, Copy, Deserialize)]
enum Sched {
    #[serde(rename = "normal")]
    Normal,
    #[serde(rename = "fifo")]
    Fifo,
    #[serde(rename = "rr")]
    RoundRobin,
    #[serde(rename = "batch")]
    Batch,
    #[serde(rename = "iso")]
    Iso,
    #[serde(rename = "idle")]
    Idle,
}

#[derive(Deserialize)]
struct Adjustments {
    nice: Option<i32>,
    ioclass: Option<Ioclass>,
    ionice: Option<i32>,
    sched: Option<Sched>,
    oom_score_adj: Option<i32>,
}

impl Adjustments {
    fn or(self, b: &Self) -> Self {
        Self {
            nice: self.nice.or(b.nice),
            ioclass: self.ioclass.or(b.ioclass),
            ionice: self.ionice.or(b.ionice),
            sched: self.sched.or(b.sched),
            oom_score_adj: self.oom_score_adj.or(b.oom_score_adj),
        }
    }
}

#[derive(Deserialize)]
struct TypeRule {
    #[serde(rename = "type")]
    name: String,
    #[serde(flatten)]
    adj: Adjustments,
}

#[derive(Deserialize)]
struct Rule {
    name: String,
    #[serde(rename = "type")]
    type_rule: Option<String>,
    #[serde(flatten)]
    adj: Adjustments,
}

const UPDATE_FREQ: Duration = Duration::from_secs(5);

lazy_static! {
    static ref TYPE_RULES: HashMap<String, Adjustments> =
        load_types().expect("Error while loading type config files");
    static ref RULES: HashMap<String, Adjustments> =
        load_rules(&TYPE_RULES).expect("Error while loading rule config files");
}

fn check_disks_schedulers() -> io::Result<()> {
    let prefix = Path::new("/sys/class/block");

    for entry in fs::read_dir(prefix)? {
        let entry = entry?;
        let name = entry.file_name();
        let name = name.to_string_lossy();
        if !name.contains("loop") && !name.contains("ram") && !name.contains("sr") {
            let mut scheduler = entry.path();
            scheduler.push(Path::new("queue/scheduler"));

            if let Ok(mut f) = File::open(scheduler) {
                let mut contents = String::new();

                if f.read_to_string(&mut contents).is_ok()
                    && !contents.contains("[cfq]")
                    && !contents.contains("[bfq]")
                    && !contents.contains("[bfq-mq]")
                {
                    println!(
                        concat!(
                            "Disk {:?} does not use cfq/bfq scheduler.",
                            "IOCLASS/IONICE will not work on it."
                        ),
                        name
                    );
                }
            }
        }
    }

    Ok(())
}

fn load_types() -> io::Result<HashMap<String, Adjustments>> {
    let mut res = HashMap::new();

    for entry in globwalk::GlobWalkerBuilder::new(CONFIG_DIR!(), "**/*.types")
        .build()
        .unwrap()
    {
        let entry = entry?;
        #[cfg(feature = "verbose")]
        println!("Load types: {}", entry.path().to_string_lossy());
        let file = File::open(entry.path())?;
        let file = BufReader::new(file);

        for line in file.lines() {
            let line = line?;

            if let Ok(type_rule) = serde_json::from_str::<TypeRule>(&line) {
                res.insert(type_rule.name, type_rule.adj);
            }
        }
    }

    Ok(res)
}

fn load_rules(types: &HashMap<String, Adjustments>) -> io::Result<HashMap<String, Adjustments>> {
    let mut res = HashMap::new();

    for entry in globwalk::GlobWalkerBuilder::new(CONFIG_DIR!(), "**/*.rules")
        .build()
        .unwrap()
    {
        let entry = entry?;
        #[cfg(feature = "verbose")]
        println!("Load rules: {}", entry.path().to_string_lossy());
        let file = File::open(entry.path())?;
        let file = BufReader::new(file);

        for line in file.lines() {
            let line = line?;

            if let Ok(mut rule) = serde_json::from_str::<Rule>(&line) {
                if let Some(t) = rule.type_rule.as_ref() {
                    if let Some(type_adj) = types.get(t) {
                        rule.adj = rule.adj.or(&type_adj);
                    }
                }

                res.insert(rule.name, rule.adj);
            }
        }
    }

    // Rules won't change once loaded, so shrink the hashmap
    res.shrink_to_fit();

    Ok(res)
}

fn get_tpids_by_path(pid_path: &Path) -> Result<Vec<u32>> {
    let mut res = vec![];

    let tasks_path = pid_path.join("task/");

    // tPID may not exist anymore, which is fine, just return the empty tid list
    let entries = if let Ok(e) = fs::read_dir(tasks_path) {
        e
    } else {
        return Ok(res);
    };

    for entry in entries {
        let entry = entry.chain_err(|| format!("Error while reading tasks in {:?}", pid_path))?;
        let candidate = entry.file_name();

        if let Ok(tpid) = candidate.to_string_lossy().parse::<u32>() {
            res.push(tpid);
        }
    }
    Ok(res)
}

fn update_nice(cmd: &str, pid: u32, tpid_list: &[u32], nice: i32) {
    if let Ok(old_nice) = libcw::get_priority(pid) {
        if old_nice != nice {
            let result = tpid_list
                .iter()
                .try_for_each(|tpid| libcw::set_priority(*tpid, nice));
            #[cfg(feature = "verbose")]
            {
                match result {
                    Ok(_) => {
                        println!(
                            "renice: {}[{}/{:?}] {} → {}",
                            cmd, pid, tpid_list, old_nice, nice
                        );
                    }
                    Err(e) => {
                        println!(
                            "Failed renice: {}[{}/{:?}] {} → {}: {}",
                            cmd, pid, tpid_list, old_nice, nice, e
                        );
                    }
                }
            }
        }
    }
}

fn update_policy(cmd: &str, pid: u32, tpid_list: &[u32], sched: Sched) {
    let new_policy = match sched {
        Sched::RoundRobin => libcw::Scheduler::Rr,
        Sched::Fifo => libcw::Scheduler::Fifo,
        Sched::Batch => libcw::Scheduler::Batch,
        Sched::Iso => libcw::Scheduler::Iso,
        Sched::Idle => libcw::Scheduler::Idle,
        Sched::Normal => libcw::Scheduler::Other,
    };
    if let Ok(old_policy) = libcw::get_scheduler(pid) {
        if new_policy != old_policy {
            if let Ok(id) = pid.try_into() {
                let result = libcw::set_scheduler(id, new_policy, 1);
                #[cfg(feature = "verbose")]
                {
                    match result {
                        Ok(_) => {
                            println!(
                                "sched: {}[{}/{:?}] {} → {}",
                                cmd, pid, tpid_list, old_policy, new_policy
                            );
                        }
                        Err(e) => {
                            println!(
                                "Failed sched: {}[{}/{:?}] {} → {}: {}",
                                cmd, pid, tpid_list, old_policy, new_policy, e
                            );
                        }
                    }
                }
            }
        }
    }
}

fn update_io(
    cmd: &str,
    pid: u32,
    tpid_list: &[u32],
    io_class: Ioclass,
    io_nice: i32,
) -> Result<()> {
    let output = Command::new("ionice")
        .arg("-p")
        .arg(pid.to_string())
        .output()
        .chain_err(|| "Failed to call ionice to read values")?
        .stdout;
    let output = String::from_utf8_lossy(&output);

    let io_props: Vec<&str> = output.split(": prio ").collect();

    let io_class_old = match io_props[0].trim() {
        "realtime" => Ioclass::Realtime,
        // Kernel treats none just like best-effort, so pretend none is best-effort
        "none" | "best-effort" => Ioclass::BestEffort,
        "idle" => Ioclass::Idle,
        _ => Ioclass::None,
    };

    let io_nice_old = io_props.get(1).and_then(|s| s.trim().parse::<i32>().ok());

    if io_class_old == io_class && Some(io_nice) == io_nice_old {
        return Ok(());
    } else if io_class_old == io_class {
        // Class is ok, need to change nice
        let mut exec = Command::new("ionice");
        exec.arg("-n").arg(io_nice.to_string()).arg("-p");
        for tpid in tpid_list {
            exec.arg(tpid.to_string());
        }
        exec.stdout(Stdio::null())
            .spawn()
            .chain_err(|| "Failed to call ionice")?
            .wait()
            .chain_err(|| "Failed to wait for ionice")?;
        #[cfg(feature = "verbose")]
        {
            let io_nice_str = if let Some(io_nice_old_str) = io_nice_old {
                format!("{}", io_nice_old_str)
            } else {
                "Unknown".to_string()
            };
            println!(
                "ionice: {}[{}/{:?}] {} → {}",
                cmd, pid, tpid_list, io_nice_str, io_nice
            );
        }
    } else if Some(io_nice) == io_nice_old {
        // Nice is ok, need to change class
        let mut exec = Command::new("ionice");
        exec.arg("-c").arg((io_class as i32).to_string()).arg("-p");
        for tpid in tpid_list {
            exec.arg(tpid.to_string());
        }
        exec.stdout(Stdio::null())
            .spawn()
            .chain_err(|| "Failed to call ionice")?
            .wait()
            .chain_err(|| "Failed to wait for ionice")?;
        #[cfg(feature = "verbose")]
        println!(
            "ioclass: {}[{}/{:?}] {} → {}",
            cmd, pid, tpid_list, io_class_old, io_class
        );
    } else {
        // Need to change both
        let mut exec = Command::new("ionice");
        exec.arg("-c")
            .arg((io_class as i32).to_string())
            .arg("-n")
            .arg(io_nice.to_string())
            .arg("-p");
        for tpid in tpid_list {
            exec.arg(tpid.to_string());
        }
        exec.stdout(Stdio::null())
            .spawn()
            .chain_err(|| "Failed to call ionice")?
            .wait()
            .chain_err(|| "Failed to wait for ionice")?;
        #[cfg(feature = "verbose")]
        {
            println!(
                "ioclass: {}[{}/{:?}] {} → {}",
                cmd, pid, tpid_list, io_class_old, io_class
            );
            let io_nice_str = if let Some(io_nice_old_str) = io_nice_old {
                format!("{}", io_nice_old_str)
            } else {
                "Unknown".to_string()
            };
            println!("ionice: {}[{}/ALL] {} → {}", cmd, pid, io_nice_str, io_nice);
        }
    }
    Ok(())
}

fn update_oom(cmd: &str, pid_path: &Path, oom_score_adj: i32) {
    let gw = globwalk::GlobWalkerBuilder::new(pid_path, "/task/**/oom_score_adj")
        .build()
        .unwrap()
        .filter_map(std::result::Result::ok);
    for osa in gw {
        let path = osa.path();
        let old_score = File::open(path)
            .ok()
            .and_then(|mut f| {
                let mut contents = String::new();
                f.read_to_string(&mut contents).map(|_| contents).ok()
            })
            .and_then(|c| c.parse::<i32>().ok());

        if old_score == Some(oom_score_adj) {
            return;
        }

        let _ = File::create(path)
            .and_then(|mut c| c.write_all(&oom_score_adj.to_string().into_bytes()))
            .and_then(|_| {
                #[cfg(feature = "verbose")]
                {
                    let old_oom_str = if let Some(old_oom_str) = old_score {
                        format!("{}", old_oom_str)
                    } else {
                        "Unknown".to_string()
                    };
                    println!(
                        "oom_score_adj: {}[{:?}] {} → {}",
                        cmd, path, old_oom_str, oom_score_adj
                    );
                }
                Ok(())
            });
    }
}

fn process_pid(pid: u32, pid_path: &Path, exe_path: &Path) -> Result<()> {
    if !pid_path.exists() {
        return Ok(());
    }

    let cmd = if let Ok(p) = exe_path.canonicalize() {
        p
    } else {
        return Ok(());
    };
    let cmd = cmd.file_name().unwrap().to_string_lossy();

    let tpid_list: Vec<u32> = get_tpids_by_path(&pid_path)?;

    if let Some(adj) = RULES.get(cmd.as_ref()) {
        if let Some(nice) = adj.nice {
            update_nice(&cmd, pid, &tpid_list, nice);
        }

        if let Some(sched) = adj.sched {
            update_policy(&cmd, pid, &tpid_list, sched);
        }

        if let (Some(io_class), Some(io_nice)) = (adj.ioclass, adj.ionice) {
            update_io(&cmd, pid, &tpid_list, io_class, io_nice)?;
        }

        if let Some(oom_score_adj) = adj.oom_score_adj {
            update_oom(&cmd, pid_path, oom_score_adj);
        }
    }

    Ok(())
}

fn err_handler(err: &impl error_chain::ChainedError) -> ! {
    println!("error: {}", err);

    for e in err.iter().skip(1) {
        println!("caused by: {}", e);
    }
    if let Some(backtrace) = error_chain::ChainedError::backtrace(err) {
        println!("backtrace: {:?}", backtrace);
    }

    ::std::process::exit(1);
}

fn main() {
    let _ = check_disks_schedulers();

    if env::var_os("NOTIFY_SOCKET").is_some() {
        Command::new("systemd-notify")
            .arg("--ready")
            .stdout(Stdio::null())
            .spawn()
            .chain_err(|| "Failed to spawn systemd-notify")
            .unwrap_or_else(|err| err_handler(&err))
            .wait()
            .chain_err(|| "Failed to wait for systemd-notify")
            .unwrap_or_else(|err| err_handler(&err));
    }

    let proc_path = Path::new("/proc/");

    let mut first_run = true;

    loop {
        let dirs: Vec<DirEntry> = fs::read_dir(proc_path)
            .chain_err(|| "Failed to read /proc/")
            .unwrap_or_else(|err| err_handler(&err))
            .filter_map(std::io::Result::ok)
            .collect();

        dirs.iter().for_each(|entry| {
            let candidate = entry.file_name();

            if let Ok(pid) = candidate.to_string_lossy().parse::<u32>() {
                let pid_path = entry.path();
                let exe_path = pid_path.join("exe");

                if let Ok(md) = fs::metadata(&pid_path) {
                    let mtime = md
                        .modified()
                        .chain_err(|| format!("Unable to read modification time of {:?}", pid_path))
                        .unwrap_or_else(|err| err_handler(&err))
                        .elapsed();

                    if let Ok(m) = mtime {
                        if exe_path.is_file() && m < 2 * UPDATE_FREQ || first_run {
                            process_pid(pid, &pid_path, &exe_path)
                                .unwrap_or_else(|err| err_handler(&err));
                        }
                    }
                }
            }
        });
        first_run = false;

        thread::sleep(UPDATE_FREQ);
    }
}
