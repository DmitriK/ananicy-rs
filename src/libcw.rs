use std::convert::TryInto;
use std::fmt::{self, Display};
use std::io::Error;

#[derive(Clone, Copy, PartialEq)]
pub enum Scheduler {
    Other = libc::SCHED_OTHER as isize,
    Fifo = libc::SCHED_FIFO as isize,
    Rr = libc::SCHED_RR as isize,
    Batch = libc::SCHED_BATCH as isize,
    Iso = 4,
    Idle = libc::SCHED_IDLE as isize,
}

impl Display for Scheduler {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Other => "Normal",
                Self::Fifo => "FIFO",
                Self::Rr => "Round Robin",
                Self::Batch => "Batch",
                Self::Iso => "Iso",
                Self::Idle => "Idle",
            }
        )
    }
}

pub fn get_priority(pid: u32) -> Result<i32, Error> {
    errno::set_errno(errno::Errno(0));
    let nice = unsafe { libc::getpriority(libc::PRIO_PROCESS as u32, pid) };
    let error = errno::errno();

    if nice == -1 && error != errno::Errno(0) {
        Err(Error::from(error))
    } else {
        Ok(nice)
    }
}

pub fn set_priority(pid: u32, prio: i32) -> Result<(), Error> {
    let err = unsafe { libc::setpriority(libc::PRIO_PROCESS as u32, pid, prio) };
    if err == 0 {
        Ok(())
    } else {
        Err(Error::from(errno::errno()))
    }
}

pub fn get_scheduler(pid: u32) -> Result<Scheduler, Error> {
    let pid = if let Ok(id) = pid.try_into() {
        id
    } else {
        return Err(Error::from(errno::Errno(libc::EINVAL)));
    };
    let raw = unsafe { libc::sched_getscheduler(pid) };

    match raw {
        -1 => Err(Error::from(errno::errno())),
        libc::SCHED_FIFO => Ok(Scheduler::Fifo),
        libc::SCHED_RR => Ok(Scheduler::Rr),
        libc::SCHED_BATCH => Ok(Scheduler::Batch),
        // ISO?
        libc::SCHED_IDLE => Ok(Scheduler::Idle),
        libc::SCHED_OTHER | _ => Ok(Scheduler::Other),
    }
}

pub fn set_scheduler(pid: i32, sched: Scheduler, prio: i32) -> Result<(), Error> {
    let prio_min = unsafe { libc::sched_get_priority_min(sched as i32) };
    if prio_min == -1 {
        return Err(Error::from(errno::errno()));
    }
    let prio_max = unsafe { libc::sched_get_priority_max(sched as i32) };
    if prio_max == -1 {
        return Err(Error::from(errno::errno()));
    }

    let mut prio = prio;
    if prio < prio_min {
        prio = prio_min;
    }
    if prio > prio_max {
        prio = prio_max;
    }

    let param = libc::sched_param {
        sched_priority: prio,
    };
    let err = unsafe { libc::sched_setscheduler(pid, sched as i32, &param) };
    if err == -1 {
        Err(Error::from(errno::errno()))
    } else {
        Ok(())
    }
}
